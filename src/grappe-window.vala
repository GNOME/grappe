[GtkTemplate (ui = "/org/gnome/Grappe/window.ui")]
public class Grappe.Window : Gtk.ApplicationWindow
{
	[GtkChild]
	private Gtk.Stack stack;

	[GtkChild]
	private Gtk.FileChooserButton executable_file_chooser_button;

	[GtkChild]
	private Gtk.Entry arguments_entry;

	[GtkChild]
	private Gtk.Widget left;

	[GtkChild]
	private Gtk.Box jobs_box;

	public Saga.Session saga_session { construct; get; }

	public Window (Saga.Session saga_session)
	{
		Object (saga_session: saga_session);
	}

	[GtkCallback]
	private void execute_button_clicked (Gtk.Button button)
	{
		/*
		var list_store = jobs_tree_view.model as Gtk.ListStore;
		Gtk.TreeIter iter;
		list_store.append (out iter);
		list_store.set (iter, 0, "Test", 0, 0);
		jobs_tree_view.model = list_store;
		*/
		jobs_box.pack_end (new JobBox ());
		stack.set_visible_child (jobs_box);

		var job_description = new Saga.JobDescription ();

		job_description.executable = executable_file_chooser_button.get_filename ();
		string[] arguments;
		assert (Shell.parse_argv (arguments_entry.text, out arguments));
		job_description.arguments = arguments;

		var service = Saga.JobService.@new (saga_session, new Saga.URL ("local://"));
		assert (service != null);
		var job = service.create_job (job_description);

		job.get_result_async.begin (Priority.DEFAULT, () => {
			var notif = new Notification ("Job %s has completed!".printf (job.job_id));
			notif.set_body ("hey!");
			Application.get_default ().send_notification (null, notif);
		});

		var job_window = new JobWindow (job);
		job_window.transient_for = this;
		job_window.show_all ();
	}
}

