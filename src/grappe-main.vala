int main (string[] args)
{
	// var builder = new Gtk.Builder.from_resource ("/org/gnome/Grappe/main.ui");
	// var window = builder.get_object ("window") as Gtk.Window;
	// window.show_all ();
	var app = new Gtk.Application ("org.gnome.Grappe",
	                               0); //GLib.ApplicationFlags.NONE);

	GLib.Environment.set_prgname ("Grappe");

	app.activate.connect (() => {
		var w = new Grappe.Window (new Saga.Session ());
		app.add_window (w);
		w.show_all ();
	});

	return app.run (args);
}
