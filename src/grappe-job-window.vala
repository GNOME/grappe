[GtkTemplate (ui = "/org/gnome/Grappe/job-window.ui")]
public class Grappe.JobWindow : Gtk.Window
{
	public Saga.Job saga_job { construct; get; }

	[GtkChild]
	private Gtk.Label job_id_label;

	[GtkChild]
	private Gtk.Label execution_hosts_label;

	[GtkChild]
	private Gtk.Label created_label;

	public JobWindow (Saga.Job saga_job)
	{
		Object (saga_job: saga_job);
	}

	construct {
		job_id_label.label = saga_job.job_id;
		execution_hosts_label.label = string.joinv (", ", saga_job.execution_hosts);
		created_label.label = saga_job.created.to_string ();
	}
}
